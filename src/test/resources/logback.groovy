import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.core.ConsoleAppender

import static ch.qos.logback.classic.Level.DEBUG
import static ch.qos.logback.classic.Level.INFO

appender("STDOUT", ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = "%d{HH:mm:ss.SSS} [%thread] %-5level %logger{5} Groovy - %msg%n"
    }
}

//def bySecond = timestamp("yyyyMMdd'T'HHmmss")

//appender("FILE", FileAppender) {
////    file = "logs/testsuite-${bySecond}.log"
//    file = 'logs/testsuite.log'
//    append = true
//    encoder(PatternLayoutEncoder) {
//        pattern = "%d{HH:mm:ss.SSS} %-5level %logger{5} Groovy - %msg%n"
//    }
//}

//appender("ROLLING", RollingFileAppender) {
//    encoder(PatternLayoutEncoder) {
//        Pattern = "%d{HH:mm:ss.SSS} %-5level %logger{5} Groovy - %msg%n"
//    }
//    rollingPolicy(TimeBasedRollingPolicy) {
//        FileNamePattern = "logs/testsuite-%d{yyyy-MM}.log"
//    }
//}

logger("fr.apec.groovy.tests.", DEBUG)
logger("fr.apec.groovy.tests.services", INFO)
root(DEBUG, ["STDOUT"])