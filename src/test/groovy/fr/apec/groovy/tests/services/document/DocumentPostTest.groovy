package fr.apec.groovy.tests.services.document

import com.sun.jersey.api.client.ClientResponse
import fr.apec.service.DocumentServiceClient
import groovy.json.JsonSlurper
import groovy.util.logging.Slf4j
import org.apache.commons.lang.RandomStringUtils
import org.junit.Test

//import fr.apec.groovy.tests.GroovyTestsUtils
/**
 * Created by pleroux on 19/09/2014.
 */
@Slf4j("logger")
class DocumentPostTest extends fr.apec.groovy.tests.GroovyTestCase {

//    RESTClient restClient = GroovyTestsUtils.getRestClient()
    RandomStringUtils random = new RandomStringUtils()

    @Test
    void testEcritureDocument() {
        logger.info "testEcritureDocument"
        def fields = ["a": "1", "b": "2", "c": "3"]
        new File("working-files/foo.log").withWriter { out ->
            fields.each() { key, value ->
                out.writeLine("${key}=${value}")
            }
        }
        assert new File("working-files/foo.log").exists()
    }

    @Test
    void testUploadDocumentIndefiniOK01() {
        String nomDocument = "working-files/foobarzoo.docx"
        logger.info "testUploadDocumentIndefiniOK01 : uploadDocument du fichier ${nomDocument}"
        File file = new File(nomDocument)
        assert file.exists()
        String nomRepAleatoire = random.randomAlphabetic(20)
        ClientResponse clientResponse = DocumentServiceClient.uploadDocument(nomDocument, nomRepAleatoire, null, "cvwords/", "([^\\s]+(\\.(?i)(doc|docx|rtf))\$)", 2048)
        logger.info "response status : ${clientResponse.getStatus()}"
        assert clientResponse.getStatus().intValue() == 201
        def slurper = new JsonSlurper()
        def response = clientResponse.getEntity(String.class)
        logger.info "response data : ${response}"
        def result = slurper.parseText(response)
        assert result.typeDocument == 'indefini'
        assert result.cheminRelatifDocumentServeur.contains('cvwords/')
        assert (result.tailleContenuEnOctets.intValue() <= (2048 * 1024))
    }

    @Test
    void testUploadDocumentIndefiniOK02() {
        String nomDocument = "working-files/foobarzoo.docx"
        logger.info "testUploadDocumentIndefiniOK02 : uploadDocument du fichier ${nomDocument}"
        File file = new File(nomDocument)
        assert file.exists()
        String nomRepAleatoire = random.randomAlphabetic(20)
        ClientResponse clientResponse = DocumentServiceClient.uploadDocument(nomDocument, nomRepAleatoire, "tyty+forced.docx", "cvwords/", "([^\\s]+(\\.(?i)(doc|docx|rtf))\$)", 2048)
        logger.info "response status : ${clientResponse.getStatus()}"
        assert clientResponse.getStatus().intValue() == 201
        def slurper = new JsonSlurper()
        def response = clientResponse.getEntity(String.class)
        logger.info "response data : ${response}"
        def result = slurper.parseText(response)
        assert result.typeDocument == 'indefini'
        assert result.cheminRelatifDocumentServeur.contains('cvwords/')
        assert result.nomDocument.contains('tyty_forced.docx')
        assert (result.tailleContenuEnOctets.intValue() <= (2048 * 1024))
    }

    @Test
    void testUploadDocumentIndefiniMauvaiseExtension() {
        String nomDocument = "working-files/pascal-brutal.jpg"
        logger.info "testUploadDocumentIndefiniMauvaiseExtension : uploadDocument du fichier ${nomDocument}"
        File file = new File(nomDocument)
        assert file.exists()
        String nomRepAleatoire = random.randomAlphabetic(20)
        ClientResponse clientResponse = DocumentServiceClient.uploadDocument(nomDocument, nomRepAleatoire, null, "images/", "([^\\s]+(\\.(?i)(doc|docx|rtf))\$)", 1)
        logger.info "response status : ${clientResponse.getStatus()}"
        assert clientResponse.getStatus().intValue() == 400
        def slurper = new JsonSlurper()
        def response = clientResponse.getEntity(String.class)
        logger.info "response data : ${response}"
        def result = slurper.parseText(response)
        assert result.filtreDocument.contains('mauvaise extention')
        assert result.tailleMaxDocument.contains('taille du fichier téléversé trop importante')
    }

    @Test
    void testUploadDocumentIndefiniFileAlreadyExists() {
        String nomDocument = "working-files/foobarzoo.docx"
        logger.info "testUploadDocumentIndefiniFileAlreadyExists : uploadDocument du fichier : ${nomDocument}"
        File file = new File(nomDocument)
        assert file.exists()
        String nomRepAleatoire = random.randomAlphabetic(20)
        ClientResponse clientResponse = DocumentServiceClient.uploadDocument(nomDocument, nomRepAleatoire, null, "cvwords/", "([^\\s]+(\\.(?i)(doc|docx|rtf))\$)", 2048)
        logger.info "response status : ${clientResponse.getStatus()}"
        assert clientResponse.getStatus().intValue() == 201
        def response = clientResponse.getEntity(String.class)
        logger.info "response data : ${response}"
        logger.info "testUploadDocumentIndefiniFileAlreadyExists : uploadDocument à nouveau du fichier : ${nomDocument}"
        file = new File(nomDocument)
        assert file.exists()
        clientResponse = DocumentServiceClient.uploadDocument(nomDocument, nomRepAleatoire, null, "cvwords/", "([^\\s]+(\\.(?i)(doc|docx|rtf))\$)", 2048)
        logger.info "response status : ${clientResponse.getStatus()}"
        assert clientResponse.getStatus().intValue() == 403
        response = clientResponse.getEntity(String.class)
        logger.info "response data : ${response}"
    }

    @Test
    void testUploadDocumentEnUneEtapeCV01() {
        String nomDocument = "working-files/foobarzoo.docx"
        logger.info "testUploadDocumentCV01 : uploadDocument du fichier ${nomDocument}"
        File file = new File(nomDocument)
        assert file.exists()
        String nomRepAleatoire = random.randomAlphabetic(20)
        ClientResponse clientResponse = DocumentServiceClient.uploadCV(nomDocument, nomRepAleatoire, null)
        logger.info "response status : ${clientResponse.getStatus()}"
        assert clientResponse.getStatus().intValue() == 201
        def slurper = new JsonSlurper()
        def response = clientResponse.getEntity(String.class)
        logger.info "response data : ${response}"
        def result = slurper.parseText(response)
        assert result.typeDocument == 'cvword'
        assert result.cheminRelatifDocumentServeur.contains('cvwords/')
        assert (result.tailleContenuEnOctets.intValue() <= (2048 * 1024))
    }

    @Test
    void testUploadDocumentStreamEnUneEtapeCV01() {
        String nomDocument = "working-files/foobarzoo.docx"
        logger.info "testUploadDocumentStreamEnUneEtapeCV01 : uploadDocument du fichier ${nomDocument}"
        File file = new File(nomDocument)
        assert file.exists()
        String nomRepAleatoire = random.randomAlphabetic(20)
        ClientResponse clientResponse = DocumentServiceClient.uploadStreamCV(new FileInputStream(file), nomDocument, nomRepAleatoire, null)
        logger.info "response status : ${clientResponse.getStatus()}"
        assert clientResponse.getStatus().intValue() == 201
        def slurper = new JsonSlurper()
        def response = clientResponse.getEntity(String.class)
        logger.info "response data : ${response}"
        def result = slurper.parseText(response)
        assert result.typeDocument == 'cvword'
        assert result.cheminRelatifDocumentServeur.contains('cvwords/')
        assert (result.tailleContenuEnOctets.intValue() <= (2048 * 1024))
    }

//    @Test
//    void testUploadDocumentCVTemporaire01() {
//        String nomDocument = "working-files/foobarzoo.docx"
//        logger.info "testUploadDocumentCVTemporaire01 : uploadDocument du fichier ${nomDocument}"
//        File file = new File(nomDocument)
//        assert file.exists()
//        String nomRepAleatoire = random.randomAlphabetic(20)
//        ClientResponse clientResponse = DocumentServiceClient.uploadCVTemporaire(nomDocument, nomRepAleatoire, null)
//        logger.info "response status : ${clientResponse.getStatus()}"
//        assert clientResponse.getStatus().intValue() == 201
//        def slurper = new JsonSlurper()
//        def response = clientResponse.getEntity(String.class)
//        logger.info "response data : ${response}"
//        def result = slurper.parseText(response)
//        assert result.typeDocument == 'cvword'
//        assert result.cheminRelatifDocumentServeur.contains('cvwords/')
//        assert (result.tailleContenuEnOctets.intValue() <= (2048 * 1024))
//    }


}
