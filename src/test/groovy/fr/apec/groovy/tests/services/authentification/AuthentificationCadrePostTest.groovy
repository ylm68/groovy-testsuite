package fr.apec.groovy.tests.services.authentification

import fr.apec.groovy.tests.services.GroovyTestsUtils
import groovy.sql.Sql

//import fr.apec.groovy.tests.GroovyTestsUtils
import groovy.util.logging.Slf4j
import groovyx.net.http.HttpResponseDecorator
import groovyx.net.http.RESTClient
import org.junit.*

/**
 * Created by pleroux on 19/09/2014.
 */
@Slf4j("logger")
class AuthentificationCadrePostTest extends fr.apec.groovy.tests.GroovyTestCase {

    RESTClient restClient = GroovyTestsUtils.getRestClient()
    static Sql db = Sql.newInstance('jdbc:oracle:thin:@devdb.apec.fr:1521:INTSOCLE', 'INT1_SOCLE', 'INT1_SOCLE', 'oracle.jdbc.driver.OracleDriver')

    void initBd() {
        logger.info "Authentification01Test : initBd"
        db.eachRow('select * from CADRE where rownum <= 10') { it ->
            logger.info "$it.ID_COMPTE -- ${it.NOM_COMPTE} --"
        }
    }

    @Test
    void testMauvaisMdp() {
        logger.info "testMauvaisMdp"
        HttpResponseDecorator response
        initBd()
        def cadres = [10490001: 'JANV', 163404219: 'LEYE']
        cadres.each { id, mdp ->
            response = restClient.post(
                    path: 'authentification/compteCadre',
                    body: ['typeAuthentifiant': 'id', 'id': id, 'motDePasse': mdp],
                    requestContentType: groovyx.net.http.ContentType.JSON)
            logger.info "response : ${response.data}"
            assert response.status == 200
            assert response.data.id == id
        }
    }


    @Test
    void testMauvaisJSON() {
        logger.info "testMauvaisJSON"
        HttpResponseDecorator response
        shouldFailWithResponse {
            response = restClient.post(
                    path: 'authentification/compteCadre',
                    body: ['typeAuthentifiant': 'id', 'id': 10490001, 'motDePasse': 'JANV', 'foobarzoo': 12],
                    requestContentType: groovyx.net.http.ContentType.JSON)
        }
        assertNotNull response
    }

    @Test(expected = Exception)
    void testMauvaisMdp2() {
        logger.info "testMauvaisMdp2"
        HttpResponseDecorator response
        def cadres = [10490001: 'JANV-MAUVAIS-MDP', 163404219: 'LEYE-MAUVAIS-MDP']
        cadres.each { id, mauvaisMdp ->
            shouldFailWithResponse {
                response = restClient.post(
                        path: 'authentification/compteCadre',
                        body: ['typeAuthentifiant': 'id', 'id': id, 'motDePasse': mauvaisMdp],
                        requestContentType: groovyx.net.http.ContentType.JSON)
                logger.info "response : ${response.data}"
            }
        }
    }

    @Test
    void testMauvaisMdp3() {
        logger.info "testMauvaisMdp3"
        HttpResponseDecorator response
        def cadres = [10490001: 'JANV-MAUVAIS-MDP', 163404219: 'LEYE-MAUVAIS-MDP']
        cadres.each { id, mauvaisMdp ->
            shouldFailWithResponse Exception, {
                response = restClient.post(
                        path: 'authentification/compteCadre',
                        body: ['typeAuthentifiant': 'id', 'id': id, 'motDePasse': mauvaisMdp],
                        requestContentType: groovyx.net.http.ContentType.JSON)
                logger.info "response : ${response.data}"
            }
        }
    }

    @Test(expected = ArithmeticException)
    void testDivisionParZero() {
        logger.info "division par zero"
        shouldFail { 12 / 0 }
    }

    @Test
    void testBD() {
        db.eachRow('select * from CADRE where rownum <= 10') { it ->
            logger.info "$it.ID_COMPTE -- ${it.NOM_COMPTE} --"
        }
        assert true
    }

    @Test
    void testOK() {
        logger.info "testOK"
        HttpResponseDecorator response
        def cadres = [10490001: 'JANV', 163404219: 'LEYE']
        cadres.each { id, mdp ->
            response = restClient.post(
                    path: 'authentification/compteCadre',
                    body: ['typeAuthentifiant': 'id', 'id': id, 'motDePasse': mdp],
                    requestContentType: groovyx.net.http.ContentType.JSON)
            logger.info "response : ${response.data}"
            assert response.status == 200
            assert response.data.id == id
        }
    }

    @Test
    void testNOK() {
        logger.info "testNOK"
        def cadres = [10490001: 'JANV', 163404219: 'LEYE']
        def message
        cadres.each { id, mdp ->
            message = shouldFailWithResponse Exception, {
                restClient.post(
                        path: 'authentification/compteCadre',
                        body: ['typeAuthentifiant': 'email', 'id': id, 'motDePasse': mdp],
                        requestContentType: groovyx.net.http.ContentType.JSON)
            }
            logger.info "message : ${message}"
            logger.info "response : ${message.response.data}"
            assert message.response.status == 400
            assert message.response.data.email == 'obligatoire'
        }
    }

    @Test
    void test2NOK() {
        logger.info "test2NOK"
        def cadres = [10490001: 'JANV', 163404219: 'LEYE']
        def message
        cadres.each { id, mdp ->
            assert shouldFailWithResponse(Exception) {
                restClient.post(
                        path: 'authentification/compteCadre',
                        body: ['typeAuthentifiant': 'email', 'id': id, 'motDePasse': mdp],
                        requestContentType: groovyx.net.http.ContentType.JSON)
            }.response.status == 400
        }
    }

}
