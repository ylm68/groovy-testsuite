package fr.apec.groovy.tests.services.document
import com.sun.jersey.api.client.ClientResponse
import fr.apec.groovy.tests.services.GroovyTestsUtils
import fr.apec.service.DocumentServiceClient
import groovy.json.JsonSlurper
import groovy.util.logging.Slf4j
import groovyx.net.http.RESTClient
import org.apache.commons.lang.RandomStringUtils
import org.junit.Test
/**
 * Created by pleroux on 30/09/2014.
 */
@Slf4j("logger")
class DocumentDeleteTest extends fr.apec.groovy.tests.GroovyTestCase {

    RESTClient restClient = GroovyTestsUtils.getRestClient()
    RandomStringUtils random = new RandomStringUtils()

    @Test
    void testDeleteDocument01OK() {
        // uploadDocument OK pour pouvoir tester le download
        String nomDocument = "working-files/pascal-brutal.jpg"
        logger.info "testDeleteDocument01OK : uploadDocument du fichier ${nomDocument}"
        File file = new File(nomDocument)
        assert file.exists()
        def taille = file.length()
        String nomRepAleatoire = random.randomAlphabetic(20)
        ClientResponse clientResponse = DocumentServiceClient.uploadDocument(nomDocument, nomRepAleatoire, "scalpa-brutal-TO-DELETE.jpg", "images/", "([^\\s]+(\\.(?i)(jpg|png|gif))\$)", 2048)
        assert clientResponse.getStatus().intValue() == 201
        def slurper = new JsonSlurper()
        def response = clientResponse.getEntity(String.class)
        logger.info "response data : ${response}"
        def result = slurper.parseText(response)
        assert result.typeDocument == 'indefini'
        def nomDocumentServeur = result.nomDocument
        assert result.tailleContenuEnOctets == taille

        // test de delete
        logger.info "testDeleteDocument01OK : deleteDocument du fichier ${nomDocumentServeur}"
        response = restClient.delete(
                path: 'document/divers',
                query: [nomDocument: nomDocumentServeur, repertoire: 'images/'],
                requestContentType: groovyx.net.http.ContentType.JSON)
        assert response.status == 200
    }

//    @Test
//    void testDeleteDocument01NOK() {
//        // testDeleteDocument01NOK on verifie que le document n'existe pas
//        logger.info "testDeleteDocument01NOK : on verifie que le document n'existe pas : doesnotexist.jpg"
//        HttpResponseDecorator response
//        shouldFailWithResponse {
//            response = restClient.get(
//                    path: 'document/divers',
//                    query: [nomDocument: 'doesnotexist.jpg', repertoire: 'images/'],
//                    requestContentType: groovyx.net.http.ContentType.JSON)
//            logger.info "response status : ${response.status}"
//        }
//        assert response.status == 404
//        // test de delete
//        logger.info "testDeleteDocument01OK : deleteDocument du fichier doesnotexist.jpg"
//        assert shouldFailWithResponse(Exception) {
//            restClient.delete(
//                    path: 'document/divers',
//                    query: [nomDocument: doesnotexist.jpg, repertoire: 'images/'],
//                    requestContentType: groovyx.net.http.ContentType.JSON)
//        }.response.status == 404
//    }

}
