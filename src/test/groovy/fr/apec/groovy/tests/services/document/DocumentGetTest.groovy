package fr.apec.groovy.tests.services.document

import com.sun.jersey.api.client.ClientResponse
import fr.apec.groovy.tests.services.GroovyTestsUtils
import fr.apec.service.DocumentServiceClient
import groovy.json.JsonSlurper
import groovy.util.logging.Slf4j
import groovyx.net.http.RESTClient
import org.apache.commons.lang.RandomStringUtils
import org.junit.Test
/**
 * Created by pleroux on 29/09/2014.
 */
@Slf4j("logger")
class DocumentGetTest extends fr.apec.groovy.tests.GroovyTestCase {

    RESTClient restClient = GroovyTestsUtils.getRestClient()
    RandomStringUtils random = new RandomStringUtils()
    static final String DOWNLOADED_FILES_DIRECTORY = "C://tmp/downloaded-files/";

    @Test
    void testEncodeDecode() {
        def test01 = '45632 1654/foo+bar Zoo.doxx'
        logger.info "original : ${test01}"
        def test02 = URLEncoder.encode(test01, 'UTF-8');
        logger.info "encodé : ${test02}"
        def test03 = URLDecoder.decode(test01, 'UTF-8');
        logger.info "encodé puis décodé : ${test03}"
        assertFalse test01.equals(test03)
        def url = restClient.getUri().toString() + test01
        logger.info "url : ${url}"
        def urlEncoded = URLEncoder.encode(url, 'UTF-8')
        def urlEncodedDecoded = URLDecoder.decode(urlEncoded, 'UTF-8')
        logger.info "urlEncoded : ${urlEncoded}"
        logger.info "urlEncodedDecoded : ${urlEncodedDecoded}"
    }

    @Test
    void testDownloadDocumentIndefiniOK01() {
        // uploadDocument OK pour pouvoir tester le download
        String nomDocument = "working-files/foo bar+zoo.docx"
        logger.info "testDownloadDocumentIndefiniOK01 : uploadDocument du fichier ${nomDocument}"
        File file = new File(nomDocument)
        assert file.exists()
        def taille = file.length()
        String nomRepAleatoire = random.randomAlphabetic(20)
        ClientResponse clientResponse = DocumentServiceClient.uploadDocument(nomDocument, nomRepAleatoire, "fooB ar+Zoo.doc", "cvwords/", "([^\\s]+(\\.(?i)(doc|docx|rtf))\$)", 2048)
        assert clientResponse.getStatus().intValue() == 201
        def slurper = new JsonSlurper()
        def response = clientResponse.getEntity(String.class)
        logger.info "response data : ${response}"
        def result = slurper.parseText(response)
        assert result.typeDocument == 'indefini'
        def nomDocumentServeur = result.nomDocument
        assert result.tailleContenuEnOctets == taille

        // test de download
        logger.info "testDownloadDocumentIndefiniOK01 : downloadDocument du fichier ${nomDocumentServeur}"
        response = restClient.get(
                path: 'document/divers',
                query : [nomDocument: nomDocumentServeur, repertoire: 'cvwords/'],
                requestContentType: groovyx.net.http.ContentType.JSON)
        assert response.status == 200
        assert response.data.typeDocument == 'indefini'
        assert response.data.nomDocument == nomDocumentServeur
        assert response.data.tailleContenuEnOctets == taille
    }

    @Test
    void testDownloadDocumentIndefiniContent01() {
        // uploadDocument OK pour pouvoir tester le download
        String nomDocument = "working-files/pascal-brutal.jpg"
        logger.info "testDownloadDocumentIndefiniContent01 : uploadDocument du fichier ${nomDocument}"
        File file = new File(nomDocument)
        assert file.exists()
        def taille = file.length()
        String nomRepAleatoire = random.randomAlphabetic(20)
        ClientResponse clientResponse = DocumentServiceClient.uploadDocument(nomDocument, nomRepAleatoire, "scalpa brutal.jpg", "images/", "([^\\s]+(\\.(?i)(jpg|png|gif))\$)", 2048)
        assert clientResponse.getStatus().intValue() == 201
        def slurper = new JsonSlurper()
        def response = clientResponse.getEntity(String.class)
        logger.info "response data : ${response}"
        def result = slurper.parseText(response)
        assert result.typeDocument == 'indefini'
        def nomDocumentServeur = result.nomDocument
        assert result.tailleContenuEnOctets == taille

        // test de download
        logger.info "testDownloadDocumentIndefiniContent01 : downloadDocument du fichier ${nomDocumentServeur}"
        clientResponse = DocumentServiceClient.downloadDocumentContent(nomDocumentServeur, "images/")
        assert clientResponse.getStatus().intValue() == 200
        File s = clientResponse.getEntity(File.class);
        File ff = new File("${DOWNLOADED_FILES_DIRECTORY}/indefini/${nomDocumentServeur}");
        File repertoire = ff.toPath().getParent().toFile();
        if (!repertoire.exists()) {
            repertoire.mkdirs();
        }
        s.renameTo(ff);
        FileWriter fr = new FileWriter(s);
        fr.flush();
        fr.close()
        ff = new File("${DOWNLOADED_FILES_DIRECTORY}/indefini/${nomDocumentServeur}");
        assert ff.length() == taille
    }

    @Test
    void testDownloadCVContent01() {
        // upload CV temporaire OK pour pouvoir tester le download
        String nomDocument = "working-files/foobarzoo.doc"
        logger.info "testDownloadCVContent01 : uploadDocument tmp du fichier ${nomDocument}"
        File file = new File(nomDocument)
        assert file.exists()
        def taille = file.length()
        String nomRepAleatoire = random.randomAlphabetic(20)
        ClientResponse clientResponse = DocumentServiceClient.uploadCVTemporaire(nomDocument, nomRepAleatoire, "zoobarfoo.doc")
        assert clientResponse.getStatus().intValue() == 201
        def slurper = new JsonSlurper()
        def response = clientResponse.getEntity(String.class)
        logger.info "response data : ${response}"
        def result = slurper.parseText(response)
        assert result.typeDocument == 'cvword'
        def nomDocumentServeur = result.nomDocument
        assert result.tailleContenuEnOctets == taille

        // deplacement de temp vers definitif
        logger.info "testDownloadCVContent01 : deplacement de temp vers definitif du fichier ${nomDocumentServeur}"
        response = restClient.put(
                path: 'document/cv/definitif',
                query : [nomDocument: nomDocumentServeur],
                requestContentType: groovyx.net.http.ContentType.JSON)
        assert response.status == 200

        // test de download
        logger.info "testDownloadCVContent01 : download du cv ${nomDocumentServeur}"
        clientResponse = DocumentServiceClient.downloadCVContent(nomDocumentServeur)
        assert clientResponse.getStatus().intValue() == 200
        File s = clientResponse.getEntity(File.class);
        File ff = new File("${DOWNLOADED_FILES_DIRECTORY}/cvwords/${nomDocumentServeur}");
        File repertoire = ff.toPath().getParent().toFile();
        if (!repertoire.exists()) {
            repertoire.mkdirs();
        }
        s.renameTo(ff);
        FileWriter fr = new FileWriter(s);
        fr.flush();
        fr.close()
        ff = new File("${DOWNLOADED_FILES_DIRECTORY}/cvwords/${nomDocumentServeur}");
        assert ff.length() == taille
    }

}
