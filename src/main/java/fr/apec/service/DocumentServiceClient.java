package fr.apec.service;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.file.FileDataBodyPart;
import com.sun.jersey.multipart.file.StreamDataBodyPart;
import fr.apec.groovy.tests.services.GroovyTestsUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URI;

/**
 * Created by pleroux on 24/09/2014.
 */
public class DocumentServiceClient {

  private static final Logger LOGGER = LoggerFactory.getLogger(DocumentServiceClient.class);

  private static URI getBaseURI() {
    LOGGER.info(GroovyTestsUtils.getBaseURL());
    return UriBuilder.fromUri(GroovyTestsUtils.getBaseURL()).build();
  }

  /**
   *
   * @param nomDocumentSurClient
   * @param repertoireSurServeur
   * @param nomDocumentSurClientObligatoire
   * @param repertoireRacineSurServeurSelonTypeDocument
   * @param filtres
   * @param tailleContenuMaxEnKiloOctets
   * @return
   * @throws FileNotFoundException
   */
  public static ClientResponse uploadDocument(String nomDocumentSurClient, String repertoireSurServeur,
      String nomDocumentSurClientObligatoire, String repertoireRacineSurServeurSelonTypeDocument, String filtres,
      Integer tailleContenuMaxEnKiloOctets) throws FileNotFoundException {

    LOGGER.trace("uploadDocument(" + nomDocumentSurClient + ")");
    ClientConfig config = new DefaultClientConfig();
    Client client = Client.create(config);
    WebResource resource = client.resource(getBaseURI()).path("document").path("divers");

    File fileToUpload = new File(nomDocumentSurClient);
    if (!fileToUpload.exists()) {
      throw new FileNotFoundException(nomDocumentSurClient + " not found");
    }

    FormDataMultiPart multiPart = new FormDataMultiPart();
    if (fileToUpload != null) {
      multiPart.bodyPart(new FileDataBodyPart("document", fileToUpload, MediaType.APPLICATION_OCTET_STREAM_TYPE));
      multiPart.bodyPart(new FormDataBodyPart("repertoireSurServeur", repertoireSurServeur));
      if (nomDocumentSurClientObligatoire != null) {
        multiPart.bodyPart(new FormDataBodyPart("nomDocumentSurClientObligatoire", nomDocumentSurClientObligatoire));
      }
      multiPart.bodyPart(new FormDataBodyPart(
          "repertoireRacineSurServeurSelonTypeDocument",
          repertoireRacineSurServeurSelonTypeDocument));
      multiPart.bodyPart(new FormDataBodyPart("filtres", filtres));
      multiPart.bodyPart(new FormDataBodyPart("tailleContenuMaxEnKiloOctets", tailleContenuMaxEnKiloOctets.toString()));
    }

    ClientResponse clientResp = resource.type(MediaType.MULTIPART_FORM_DATA_TYPE).post(ClientResponse.class, multiPart);
    LOGGER.trace("Response status : " + clientResp.getStatus());
    client.destroy();
    return clientResp;
  }

  // /**
  // *
  // * @param nomDocument
  // * @param repertoireSurServeur
  // * @return
  // */
  // public static ClientResponse downloadDocument(String nomDocument, String
  // repertoireSurServeur) {
  //
  // LOGGER.trace("downloadDocument(" + nomDocument +
  // ") dans répertoire serveur : " + repertoireSurServeur);
  // ClientConfig config = new DefaultClientConfig();
  // Client client = Client.create(config);
  // WebResource resource =
  // client.resource(getBaseURI()).path("document").path("divers").queryParam("nomDocument",
  // nomDocument).queryParam(
  // "repertoire",
  // repertoireSurServeur);
  // ClientResponse clientResp = resource.get(ClientResponse.class);
  // LOGGER.trace("Response status : " + clientResp.getStatus());
  // client.destroy();
  // return clientResp;
  // }

  /**
   *
   * @param nomDocument
   * @param repertoireSurServeur
   * @return
   */
  public static ClientResponse downloadDocumentContent(String nomDocument, String repertoireSurServeur) {

    LOGGER.trace("downloadDocument(" + nomDocument + ") dans répertoire serveur : " + repertoireSurServeur);
    ClientConfig config = new DefaultClientConfig();
    Client client = Client.create(config);
    WebResource resource =
        client.resource(getBaseURI()).path("document").path("divers").path("contenu").queryParam(
            "nomDocument",
            nomDocument).queryParam("repertoire", repertoireSurServeur);
    ClientResponse clientResp = resource.get(ClientResponse.class);
    LOGGER.trace("Response status : " + clientResp.getStatus());
    client.destroy();
    return clientResp;
  }

  // /**
  // *
  // * @param nomDocument
  // * @param repertoireSurServeur
  // * @return
  // */
  // public static ClientResponse deleteDocument(String nomDocument, String
  // repertoireSurServeur) {
  //
  // LOGGER.trace("deleteDocument(" + nomDocument +
  // ") dans répertoire serveur : " + repertoireSurServeur);
  // ClientConfig config = new DefaultClientConfig();
  // Client client = Client.create(config);
  // WebResource resource =
  // client.resource(getBaseURI()).path("document").path("divers").queryParam("nomDocument",
  // nomDocument).queryParam(
  // "repertoire",
  // repertoireSurServeur);
  // ClientResponse clientResp = resource.delete(ClientResponse.class);
  // LOGGER.trace("Response status : " + clientResp.getStatus());
  // client.destroy();
  // return clientResp;
  // }

  /**
   *
   * @param nomDocumentSurClient
   * @param repertoireSurServeur
   * @param nomDocumentSurClientObligatoire
   * @return
   * @throws FileNotFoundException
   */
  public static ClientResponse uploadCV(String nomDocumentSurClient, String repertoireSurServeur,
      String nomDocumentSurClientObligatoire) throws FileNotFoundException {

    LOGGER.trace("uploadCV(" + nomDocumentSurClient + ")");
    ClientConfig config = new DefaultClientConfig();
    Client client = Client.create(config);
    WebResource resource = client.resource(getBaseURI()).path("document").path("cv");

    File fileToUpload = new File(nomDocumentSurClient);
    if (!fileToUpload.exists()) {
      throw new FileNotFoundException(nomDocumentSurClient + " not found");
    }

    FormDataMultiPart multiPart = new FormDataMultiPart();
    if (fileToUpload != null) {
      multiPart.bodyPart(new FileDataBodyPart("document", fileToUpload, MediaType.APPLICATION_OCTET_STREAM_TYPE));
      multiPart.bodyPart(new FormDataBodyPart("repertoireSurServeur", repertoireSurServeur));
      if (nomDocumentSurClientObligatoire != null) {
        multiPart.bodyPart(new FormDataBodyPart("nomDocumentSurClientObligatoire", nomDocumentSurClientObligatoire));
      }
    }

    ClientResponse clientResp = resource.type(MediaType.MULTIPART_FORM_DATA_TYPE).post(ClientResponse.class, multiPart);
    LOGGER.trace("Response status : " + clientResp.getStatus());
    client.destroy();
    return clientResp;
  }

  /**
   *
   * @param nomDocumentSurClient
   * @param repertoireSurServeur
   * @param nomDocumentSurClientObligatoire
   * @return
   * @throws FileNotFoundException
   */
  public static ClientResponse uploadStreamCV(InputStream fileInputStream, String nomDocumentSurClient, String repertoireSurServeur,
                                        String nomDocumentSurClientObligatoire) throws FileNotFoundException {

    LOGGER.trace("uploadStreamCV(" + nomDocumentSurClient + ")");
    ClientConfig config = new DefaultClientConfig();
    Client client = Client.create(config);
    WebResource resource = client.resource(getBaseURI()).path("document").path("cv");

    File fileToUpload = new File(nomDocumentSurClient);
    if (!fileToUpload.exists()) {
      throw new FileNotFoundException(nomDocumentSurClient + " not found");
    }

    FormDataMultiPart multiPart = new FormDataMultiPart();
    if (fileToUpload != null) {
      multiPart.bodyPart(new StreamDataBodyPart("document", fileInputStream, nomDocumentSurClient, MediaType.APPLICATION_OCTET_STREAM_TYPE));
      multiPart.bodyPart(new FormDataBodyPart("repertoireSurServeur", repertoireSurServeur));
      if (nomDocumentSurClientObligatoire != null) {
        multiPart.bodyPart(new FormDataBodyPart("nomDocumentSurClientObligatoire", nomDocumentSurClientObligatoire));
      }
    }

    ClientResponse clientResp = resource.type(MediaType.MULTIPART_FORM_DATA_TYPE).post(ClientResponse.class, multiPart);
    LOGGER.trace("Response status : " + clientResp.getStatus());
    client.destroy();
    return clientResp;
  }

  /**
   *
   * @param nomDocumentSurClient
   * @param repertoireSurServeur
   * @param nomDocumentSurClientObligatoire
   * @return
   * @throws FileNotFoundException
   */
  public static ClientResponse uploadCVTemporaire(String nomDocumentSurClient, String repertoireSurServeur,
      String nomDocumentSurClientObligatoire) throws FileNotFoundException {

    LOGGER.trace("uploadCV(" + nomDocumentSurClient + ")");
    ClientConfig config = new DefaultClientConfig();
    Client client = Client.create(config);
    WebResource resource = client.resource(getBaseURI()).path("document").path("cv").path("temporaire");

    File fileToUpload = new File(nomDocumentSurClient);
    if (!fileToUpload.exists()) {
      throw new FileNotFoundException(nomDocumentSurClient + " not found");
    }

    FormDataMultiPart multiPart = new FormDataMultiPart();
    if (fileToUpload != null) {
      multiPart.bodyPart(new FileDataBodyPart("document", fileToUpload, MediaType.APPLICATION_OCTET_STREAM_TYPE));
      multiPart.bodyPart(new FormDataBodyPart("repertoireSurServeur", repertoireSurServeur));
      if (nomDocumentSurClientObligatoire != null) {
        multiPart.bodyPart(new FormDataBodyPart("nomDocumentSurClientObligatoire", nomDocumentSurClientObligatoire));
      }
    }

    ClientResponse clientResp = resource.type(MediaType.MULTIPART_FORM_DATA_TYPE).post(ClientResponse.class, multiPart);
    LOGGER.trace("Response status : " + clientResp.getStatus());
    client.destroy();
    return clientResp;
  }

  /**
   *
   * @param nomDocument
   * @return
   */
  public static ClientResponse downloadCVContent(String nomDocument) {

    LOGGER.trace("downloadDocument(" + nomDocument + ")");
    ClientConfig config = new DefaultClientConfig();
    Client client = Client.create(config);
    WebResource resource =
        client.resource(getBaseURI()).path("document").path("cv").path("contenu").queryParam("nomDocument", nomDocument);
    ClientResponse clientResp = resource.get(ClientResponse.class);
    LOGGER.trace("Response status : " + clientResp.getStatus());
    client.destroy();
    return clientResp;
  }

}
