package fr.apec.groovy.tests.services

import groovy.util.logging.Slf4j
import groovyx.net.http.RESTClient
/**
 * Created by pleroux on 19/09/2014.
 */

@Slf4j(value = "logger")
class GroovyTestsUtils {

//    static String BaseURL = "http://localhost:8080/rest-server/rest/"
    static String BaseURL = "http://wsmint1.pprod-apec.fr:8080/rest-server/rest/"
//    static String BaseURL = "http://192.168.14.73:8080/rest-server/rest/"

    static RESTClient RESTClient = new RESTClient(BaseURL)

    static RESTClient getRestClient() {
        if (RESTClient == null) {
            logger.info "init REST Client pour : ${BaseURL}"
            RESTClient = new RESTClient(BaseURL)
        }
        return RESTClient
    }

}
