package fr.apec.groovy.tests

import groovy.util.logging.Slf4j
/**
 * Created by pleroux on 22/09/2014.
 */
@Slf4j(value = "logger")
abstract class GroovyTestCase extends groovy.util.GroovyTestCase{

    protected Object shouldFailWithResponse(Class<Exception> expectedExceptionClass = Exception, Closure closure) {
        try {
            closure()
        } catch(e) {
            if (!expectedExceptionClass.isInstance(e)) {
                logger.debug "Expected exception $expectedExceptionClass was not thrown"
                throw new AssertionError("Expected exception $expectedExceptionClass was not thrown")
            }
            logger.info "Closure Failed :-)"
            logger.info "response : ${e.getMessage()}"
            return e //.getMessage()
        }
        logger.debug "Expected exception $expectedExceptionClass was not thrown"
        throw new AssertionError("Expected exception $expectedExceptionClass was not thrown")
    }

}
